package com.blogspot.nataliprograms.androidtestapplication.ui.handlers;

import android.view.View;

/**
 * Click Handler for Todolist items using data binding
 */

public interface TodoItemClickHandler {

    void onCheckBoxSelected(View view);
    void onCheckBoxUnSelected(View view);
    void onTodoItemSelected(View view);

}
