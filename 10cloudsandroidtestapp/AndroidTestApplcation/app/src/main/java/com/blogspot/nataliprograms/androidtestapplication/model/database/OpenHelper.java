package com.blogspot.nataliprograms.androidtestapplication.model.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.blogspot.nataliprograms.androidtestapplication.model.database.tables.Todo;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Database create and update functionality
 */

public class OpenHelper extends OrmLiteSqliteOpenHelper {


    public OpenHelper(Context context)
    {
        super(context,
                TestAppContract.DATABASE_NAME,
                null,
                TestAppContract.DATABASE_VERSION
        );
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource)
    {
        try
        {
            TableUtils.createTableIfNotExists(connectionSource, Todo.class);
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion)
    {
        //TODO All user data will be lost
        try
        {
            TableUtils.dropTable(connectionSource, Todo.class, true);
            TableUtils.createTable(connectionSource, Todo.class);
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
    }
}
