package com.blogspot.nataliprograms.androidtestapplication.model.database.tables;

import android.provider.BaseColumns;

import com.blogspot.nataliprograms.androidtestapplication.model.database.TestAppContract;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentUri;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentMimeTypeVnd;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultSortOrder;

import java.util.ArrayList;

/**
 * POJO object for "todo" item
 * Uses as json and database scheme
 */

@DatabaseTable(tableName = TestAppContract.Todo.TABLE_NAME)
@DefaultContentUri(authority = TestAppContract.AUTHORITY, path = TestAppContract.Todo.CONTENT_URI_PATH)
@DefaultContentMimeTypeVnd(name = TestAppContract.Todo.MIMETYPE_NAME, type = TestAppContract.Todo.MIMETYPE_TYPE)
public class Todo {

    //Default id for content provider
    @DatabaseField(columnName = BaseColumns._ID, generatedId = true)
    @DefaultSortOrder
    private int default_id;

    @DatabaseField(canBeNull = false, dataType = DataType.INTEGER)
    @SerializedName("id")
    private int todoId;

    @DatabaseField(canBeNull = false, dataType = DataType.INTEGER)
    private int userId;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING)
    private String title;

    @DatabaseField(canBeNull = false, dataType = DataType.BOOLEAN)
    @SerializedName("completed")
    private boolean isCompleted;

    @DatabaseField(dataType = DataType.BOOLEAN, defaultValue = "false")
    private boolean isEdited;

    public Todo() {
    }

    public Todo(int id, String title, boolean isCompleted) {
        this.todoId = id;
        this.title = title;
        this.isCompleted = isCompleted;
    }

    public Todo(int id, int userId, String title, boolean icCompleted, boolean isEdited) {
        this.todoId = id;
        this.userId = userId;
        this.title = title;
        this.isCompleted = icCompleted;
        this.isEdited = isEdited;
    }

    public static class List extends ArrayList<Todo> {
    }

    public int getId() {
        return todoId;
    }

    public void setId(int id) {
        this.todoId = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public boolean isEdited() {
        return isEdited;
    }

    public void setEdited(boolean edited) {
        isEdited = edited;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "default_id=" + default_id +
                ", todoId=" + todoId +
                ", userId=" + userId +
                ", title='" + title + '\'' +
                ", isCompleted=" + isCompleted +
                ", isEdited=" + isEdited +
                '}';
    }
}
