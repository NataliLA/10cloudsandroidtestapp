package com.blogspot.nataliprograms.androidtestapplication.model.networking;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

public class RetrofitSpiceService extends RetrofitGsonSpiceService {

    private final static String JSON_PLACEHOLDER_URL = "https://jsonplaceholder.typicode.com/";

    @Override
    protected String getServerUrl() {
        return JSON_PLACEHOLDER_URL;
    }
}
