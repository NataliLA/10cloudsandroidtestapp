package com.blogspot.nataliprograms.androidtestapplication.model.networking.requests;

import com.blogspot.nataliprograms.androidtestapplication.model.networking.JsonPlaceHolderAPICalls;
import com.blogspot.nataliprograms.androidtestapplication.model.database.tables.Todo;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Retrofit based Robospice request class for downloading list of todo objects
 */

public class TodoListRequest extends RetrofitSpiceRequest<Todo.List, JsonPlaceHolderAPICalls> {

    public TodoListRequest() {
        super(Todo.List.class, JsonPlaceHolderAPICalls.class);
    }

    @Override
    public Todo.List loadDataFromNetwork() throws Exception {
        return getService().listTodos();
    }
}
