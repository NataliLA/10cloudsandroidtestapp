package com.blogspot.nataliprograms.androidtestapplication.model.database;

import android.net.Uri;

import com.blogspot.nataliprograms.androidtestapplication.model.database.tables.Todo;
import com.tojc.ormlite.android.OrmLiteSimpleContentProvider;
import com.tojc.ormlite.android.framework.MatcherController;
import com.tojc.ormlite.android.framework.MimeTypeVnd.SubType;

/**
 * Content provider for TestAppData database
 */

public class TestAppContentProvider extends OrmLiteSimpleContentProvider<OpenHelper> {

    @Override
    protected Class<OpenHelper> getHelperClass() {
        return OpenHelper.class;
    }

    @Override
    public boolean onCreate() {
        setMatcherController(new MatcherController()
                .add(Todo.class, SubType.DIRECTORY, "", TestAppContract.Todo.CONTENT_URI_PATTERN_MANY)
                .add(Todo.class, SubType.ITEM, "#", TestAppContract.Todo.CONTENT_URI_PATTERN_ONE)
        );
        return true;
    }
}
