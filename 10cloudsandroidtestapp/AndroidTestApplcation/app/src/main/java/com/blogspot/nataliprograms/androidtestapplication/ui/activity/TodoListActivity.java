package com.blogspot.nataliprograms.androidtestapplication.ui.activity;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.nataliprograms.androidtestapplication.R;
import com.blogspot.nataliprograms.androidtestapplication.databinding.ItemTodoListBinding;
import com.blogspot.nataliprograms.androidtestapplication.model.database.TestAppContract;
import com.blogspot.nataliprograms.androidtestapplication.model.database.tables.Todo;
import com.blogspot.nataliprograms.androidtestapplication.model.networking.RetrofitSpiceService;
import com.blogspot.nataliprograms.androidtestapplication.model.networking.requests.TodoListRequest;
import com.blogspot.nataliprograms.androidtestapplication.model.networking.requests.UpdateTodoItemRequest;
import com.blogspot.nataliprograms.androidtestapplication.ui.custom.EndlessRecyclerViewScrollListener;
import com.blogspot.nataliprograms.androidtestapplication.ui.handlers.TodoItemClickHandler;
import com.blogspot.nataliprograms.androidtestapplication.ui.util.DialogUtil;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import roboguice.util.temp.Ln;

public class TodoListActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private class TodoRecyclerAdapter extends RecyclerView.Adapter<TodoItemViewHolder> {

        @NonNull
        private Todo.List items;

        TodoRecyclerAdapter(@NonNull Todo.List items) {
            this.items = items;
        }

        @NonNull
        Todo.List getItems() {
            return items;
        }

        @Override
        public TodoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            ItemTodoListBinding binding = ItemTodoListBinding.inflate(inflater, parent, false);
            return new TodoItemViewHolder(binding.getRoot());
        }

        @Override
        public void onBindViewHolder(final TodoItemViewHolder holder, int position) {
            final Todo todo = items.get(position);
            holder.itemId = todo.getId();
            holder.itemTodoListBinding.setTodo(todo);
            holder.itemTodoListBinding.setListener(new TodoItemClickHandler() {
                @Override
                public void onCheckBoxSelected(View view) {
                    setTodoItemEdited(holder.itemId, holder.getAdapterPosition(), true, todo.getTitle());
                }

                @Override
                public void onCheckBoxUnSelected(View view) {
                    setTodoItemEdited(holder.itemId, holder.getAdapterPosition(), false, todo.getTitle());
                }

                @Override
                public void onTodoItemSelected(View view) {
                    DialogUtil.showTextEditDialog(view.getContext(), R.string.dialog_edit_title_message, todo.getTitle(), new DialogUtil.TextEditDialogResultListener() {
                        @Override
                        public void getInputTest(@NonNull String text) {
                            if (!todo.getTitle().equals(text)) {
                                setTodoItemEdited(holder.itemId, holder.getAdapterPosition(), todo.isCompleted(), text);
                            }
                        }
                    });
                }
            });


        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }

    private class TodoItemViewHolder extends RecyclerView.ViewHolder {

        int itemId;
        ItemTodoListBinding itemTodoListBinding;

        TodoItemViewHolder(View itemView) {
            super(itemView);
            itemTodoListBinding = DataBindingUtil.bind(itemView);
        }
    }

    /**
     * Robospice based request result listener for todolist GET request
     */
    public final class TodoListRequestListener implements RequestListener<Todo.List> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.e(TAG, spiceException.getMessage());
            DialogUtil.showOkDialog(TodoListActivity.this, R.string.dialog_notification_title, R.string.dialog_error_message);
        }

        @Override
        public void onRequestSuccess(Todo.List todos) {
            addTodoListToDB(todos);
        }
    }

    /**
     * Robospice based request result listener for todoitem PATCH request
     */
    public final class TodoUpdateRequestListener implements RequestListener<Todo> {

        private boolean isLast;

        public TodoUpdateRequestListener(boolean isLast) {
            this.isLast = isLast;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.e(TAG, spiceException.getMessage());
            if (mProgressDialog.isShowing()) {
                mProgressDialog.cancel();
            }
            DialogUtil.showOkDialog(TodoListActivity.this, R.string.dialog_notification_title, R.string.dialog_error_message);
        }

        @Override
        public void onRequestSuccess(@NonNull final Todo todo) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    // Set updated item state as non edited
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(TestAppContract.Todo.IS_EDITED, false);
                    getContentResolver().update(TestAppContract.Todo.contentUri,
                            contentValues, TestAppContract.Todo.TODO_ID + "= " + todo.getId(), null);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // refresh all recycler view data
                            if (isLast) {
                                setRecyclerViewAdapter(new Todo.List());
                                currentPage = 0;
                                getSupportLoaderManager().restartLoader(0, null, TodoListActivity.this);
                            }
                        }
                    });
                }
            }).start();
        }
    }

    private static final String TAG = "TodoListActivity";
    public static final int PAGE_SIZE = 10;

    private SpiceManager mSpiceManager = new SpiceManager(RetrofitSpiceService.class);

    private RecyclerView mRecyclerView;
    private ProgressDialog mProgressDialog;

    private RetrofitSpiceRequest mRetrofitSpiceRequest;

    //last downloaded
    private int currentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_list);

        mRecyclerView = (RecyclerView) findViewById(R.id.todo_list_recycler_view);

        mProgressDialog = DialogUtil.createProgressDialog(this, R.string.dialog_loading_message);

        mRetrofitSpiceRequest = new TodoListRequest();

        //Setup RecyclerView data
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        setRecyclerViewAdapter(new Todo.List());
        mRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {

            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                currentPage = page;
                Log.v(TAG, "load page = " + page);
                Log.v(TAG, "totalItemsCount = " + totalItemsCount);
                getSupportLoaderManager().restartLoader(0, null, TodoListActivity.this);
            }
        });

        currentPage = 0;

        getSupportLoaderManager().initLoader(0, null, this);
        mProgressDialog.show();
        //Clear table data (offline mode not supported)
        getContentResolver().delete(TestAppContract.Todo.contentUri, null, null);
    }

    @Override
    protected void onStart() {
        mSpiceManager.start(this);
        mSpiceManager.execute(mRetrofitSpiceRequest, new TodoListRequestListener());
        super.onStart();
    }

    @Override
    protected void onStop() {
        mSpiceManager.shouldStop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getSupportLoaderManager().destroyLoader(0);
    }

    private void setRecyclerViewAdapter(Todo.List todos) {
        mRecyclerView.setAdapter(new TodoRecyclerAdapter(todos));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.menu_save_edits_title).setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                DialogUtil.showYesNoDialog(this, R.string.dialog_edit_title_message, R.string.dialog_save_edit_message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveEditedItems();
                    }
                });
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        //Show notification that unsaved changes will be lost
        DialogUtil.showYesNoDialog(this, R.string.dialog_notification_title, R.string.dialog_warning_edit_message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveEditedItems();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        //id is not used as only one request is used
        Uri CONTENT_URI = TestAppContract.Todo.contentUri;
        return new CursorLoader(this, CONTENT_URI, TodoListRequestData.PROJECTION, null, null,
                TestAppContract.Todo.TODO_ID + " ASC " + " LIMIT " + PAGE_SIZE * currentPage + ", " + PAGE_SIZE);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Todo.List cursorDataList = ((TodoRecyclerAdapter) mRecyclerView.getAdapter()).getItems();
        int startIndex = cursorDataList.size();
        if (data != null && data.getCount() > 0) {
            for (data.moveToFirst(); !data.isAfterLast(); data.moveToNext()) {
                cursorDataList.add(new Todo(data.getInt(TodoListRequestData.TODO_ID_ID), data.getInt(TodoListRequestData.USER_ID_ID), data.getString(TodoListRequestData.TITLE_ID),
                        data.getInt(TodoListRequestData.IS_COMPLETED_ID) > 0, data.getInt(TodoListRequestData.IS_EDITED_ID) > 0));
            }
            mRecyclerView.getAdapter().notifyItemRangeChanged(startIndex, data.getCount());
        }
        if (mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    //Todolist request and cursor params
    private interface TodoListRequestData {

        String[] PROJECTION = new String[]{TestAppContract.Todo.TODO_ID, TestAppContract.Todo.USER_ID,
                TestAppContract.Todo.TITLE, TestAppContract.Todo.IS_COMPILED, TestAppContract.Todo.IS_EDITED};

        int TODO_ID_ID = 0;
        int USER_ID_ID = 1;
        int TITLE_ID = 2;
        int IS_COMPLETED_ID = 3;
        int IS_EDITED_ID = 4;

    }

    //Todoitem update request and cursor params
    private interface TodoUpdateRequestData {

        String[] PROJECTION = new String[]{TestAppContract.Todo.TODO_ID, TestAppContract.Todo.TITLE,
                TestAppContract.Todo.IS_COMPILED};

        int TODO_ID_ID = 0;
        int TITLE_ID = 1;
        int IS_COMPLETED_ID = 2;

    }

    /**
     * Get items marked as edited from ad and send updated data to the API
     */
    private void saveEditedItems() {
        mProgressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                //Get all items marked as edited
                final Cursor cursor = getContentResolver()
                        .query(TestAppContract.Todo.contentUri, TodoUpdateRequestData.PROJECTION, TestAppContract.Todo.IS_EDITED + " = 1", null, null);
                if (cursor != null && cursor.getCount() > 0) {
                    try {
                        cursor.moveToFirst();
                        do {
                            // Save changes to the API
                            Todo todoUpdateData = new Todo(cursor.getInt(TodoUpdateRequestData.TODO_ID_ID),
                                    cursor.getString(TodoUpdateRequestData.TITLE_ID), cursor.getInt(TodoUpdateRequestData.IS_COMPLETED_ID) > 0);
                            mSpiceManager.execute(new UpdateTodoItemRequest(todoUpdateData), new TodoUpdateRequestListener(cursor.isLast()));
                        } while (cursor.moveToNext());
                    } finally {
                        if (!cursor.isClosed()) {
                            cursor.close();
                        }
                    }
                } else{
                    mProgressDialog.cancel();
                }
            }
        }).start();
    }

    /**
     * Insert list of todoitems to database
     *
     * @param todos
     */
    private void addTodoListToDB(@NonNull Todo.List todos) {
        ContentValues[] valueList = new ContentValues[todos.size()];
        int i = 0;
        for (Todo todo : todos) {
            ContentValues values = new ContentValues();
            values.clear();
            values.put(TestAppContract.Todo.TODO_ID, todo.getId());
            values.put(TestAppContract.Todo.USER_ID, todo.getUserId());
            values.put(TestAppContract.Todo.TITLE, todo.getTitle());
            values.put(TestAppContract.Todo.IS_COMPILED, todo.isCompleted());
            valueList[i++] = values;
        }
        getContentResolver().bulkInsert(TestAppContract.Todo.contentUri, valueList);
    }

    /**
     * Mark todoitem as edited in database
     *
     * @param itemId
     * @param position    item position in RecyclerView
     * @param isCompleted
     * @param title
     */
    private void setTodoItemEdited(int itemId, int position, boolean isCompleted, String title) {
        ContentValues values = new ContentValues();
        values.put(TestAppContract.Todo.IS_EDITED, true);
        values.put(TestAppContract.Todo.IS_COMPILED, isCompleted);
        values.put(TestAppContract.Todo.TITLE, title);
        getContentResolver().update(TestAppContract.Todo.contentUri, values, TestAppContract.Todo.TODO_ID + "= " + itemId, null);

        Todo todoItem = ((TodoRecyclerAdapter) mRecyclerView.getAdapter()).getItems().get(position);
        todoItem.setCompleted(isCompleted);
        todoItem.setEdited(true);
        todoItem.setTitle(title);
        mRecyclerView.getAdapter().notifyItemChanged(position);
    }
}
