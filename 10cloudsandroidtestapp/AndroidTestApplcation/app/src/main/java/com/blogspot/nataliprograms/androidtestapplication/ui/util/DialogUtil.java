package com.blogspot.nataliprograms.androidtestapplication.ui.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.widget.EditText;

import com.blogspot.nataliprograms.androidtestapplication.R;

/**
 * Class for creating alert dialogs
 */

public class DialogUtil {

    /**
     * Show dialog with "Yes"/"No" buttons
     * @param context
     * @param titleId string resource id
     * @param messageId string resource id
     * @param positiveListener "Yes" button selection listener
     */
    public static void showYesNoDialog(@NonNull Context context, @StringRes int titleId, @StringRes int messageId, @NonNull DialogInterface.OnClickListener positiveListener) {
        showYesNoDialog(context, titleId, messageId, positiveListener, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
    }

    /**
     * Show dialog with "Yes"/"No" buttons
     * @param context
     * @param titleId string resource id
     * @param messageId string resource id
     * @param positiveListener "Yes" button selection listener
     * @param negativeListener "No" button selection listener
     */
    public static void showYesNoDialog(@NonNull Context context, @StringRes int titleId, @StringRes int messageId,
                                       @NonNull DialogInterface.OnClickListener positiveListener, @NonNull DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titleId);
        builder.setMessage(messageId);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setPositiveButton(R.string.dialog_yes_button, positiveListener);
        builder.setNegativeButton(R.string.dialog_no_button, negativeListener);
        builder.show();
    }

    /**
     * Show dialog with EditText
     * @param context
     * @param titleId string resource id
     * @param defaultValue string to be preset in EditText
     * @param resultListener listener to receive text edit result
     */
    public static void showTextEditDialog(@NonNull Context context, @StringRes int titleId, @Nullable String defaultValue, @NonNull final TextEditDialogResultListener resultListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titleId);
        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(defaultValue);
        builder.setView(input);
        builder.setPositiveButton(R.string.dialog_save_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                resultListener.getInputTest(input.getText().toString());
                dialog.cancel();
            }
        });
        builder.setNegativeButton(R.string.dialog_cancel_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    /**
     * Show dialog with one Ok button
     * Pressing the Ok button will cancel the dialog
     * @param context
     * @param titleId string resource id
     * @param messageId string resource id
     */
    public static void showOkDialog(@NonNull Context context, @StringRes int titleId, @StringRes int messageId){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titleId);
        builder.setMessage(messageId);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setPositiveButton(R.string.dialog_ok_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
    }

    /**
     * Create default progress dialog
     * (cancelable = false)
     * @param context
     * @param titleId string resource id
     * @return dialog instance
     */
    public static ProgressDialog createProgressDialog(@NonNull Context context, @StringRes int titleId){
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(titleId);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    public interface TextEditDialogResultListener {
        void getInputTest(@NonNull String text);
    }
}

