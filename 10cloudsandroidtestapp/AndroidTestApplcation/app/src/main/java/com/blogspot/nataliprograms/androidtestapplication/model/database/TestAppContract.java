package com.blogspot.nataliprograms.androidtestapplication.model.database;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * TestAppData database content provider contract
 */

public class TestAppContract {

    public static final String DATABASE_NAME = "TestAppData";
    public static final int DATABASE_VERSION = 2;

    public static final String AUTHORITY = "com.blogspot.nataliprograms.androidtestapplication";

    public static class Todo implements BaseColumns {
        public static final String TABLE_NAME = "todos";

        public static final String CONTENT_URI_PATH = TABLE_NAME;

        public static final String MIMETYPE_TYPE = TABLE_NAME;
        public static final String MIMETYPE_NAME = AUTHORITY + ".provider";

        public static final String TODO_ID = "todoId";
        public static final String USER_ID = "userId";
        public static final String TITLE = "title";
        public static final String IS_COMPILED = "isCompleted";
        public static final String IS_EDITED = "isEdited";

        public static final int CONTENT_URI_PATTERN_MANY = 1;
        public static final int CONTENT_URI_PATTERN_ONE = 2;

        public static final Uri contentUri = new Uri.Builder()
                .scheme(ContentResolver.SCHEME_CONTENT)
                .authority(AUTHORITY)
                .appendPath(CONTENT_URI_PATH)
                .build();
    }
}
