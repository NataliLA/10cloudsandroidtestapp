package com.blogspot.nataliprograms.androidtestapplication.model.networking;

import android.support.annotation.NonNull;

import com.blogspot.nataliprograms.androidtestapplication.model.database.tables.Todo;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.PATCH;
import retrofit.http.Path;

public interface JsonPlaceHolderAPICalls {

    //List data request
    @GET("/todos")
    Todo.List listTodos();

    //Update data request
    @PATCH("/todos/{todoID}")
    Todo updateTodo(@NonNull @Body Todo updatedData, @Path("todoID") int todoID);
}
