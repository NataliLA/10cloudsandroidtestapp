package com.blogspot.nataliprograms.androidtestapplication.model.networking.requests;

import android.support.annotation.NonNull;

import com.blogspot.nataliprograms.androidtestapplication.model.networking.JsonPlaceHolderAPICalls;
import com.blogspot.nataliprograms.androidtestapplication.model.database.tables.Todo;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Retrofit based Robospice request class for updating todo objects data
 */

public class UpdateTodoItemRequest extends RetrofitSpiceRequest<Todo, JsonPlaceHolderAPICalls> {

    private Todo updatedData;

    public UpdateTodoItemRequest(@NonNull Todo updatedData) {
        super(Todo.class, JsonPlaceHolderAPICalls.class);
        this.updatedData = updatedData;
    }

    @Override
    public Todo loadDataFromNetwork() throws Exception {
        return getService().updateTodo(updatedData, updatedData.getId());
    }
}
