# README #

Android Test Application for 10clouds.com

Created by Nataliia Lapshyna

Known issues:
1. After double checking/unhecking checkbox, the item will be still marked as edited, but it should not be if current state is the same as after the init.
2. User id data is ignored - all todo items for all users are shown in the list
3. Save data dialog always appears after the back button pressed.
4. No tests

Unfortunately, I can't spend more time for this app to fix that issues.

Features:
Edited items are marked by orange background.
Save edited items function is available from the menu.

![screen.png](https://bitbucket.org/repo/koB8zR/images/3061461290-screen.png)